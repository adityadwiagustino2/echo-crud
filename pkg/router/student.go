package router

import (
	"crud/pkg/controller"
	"crud/pkg/repository"
	"crud/pkg/usecase"
	"database/sql"
	"github.com/labstack/echo/v4"
)

func NewStudentRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	sr := repository.NewStudentRepository(db)
	su := usecase.NewStudentUsecase(sr)
	sc := &controller.StudentControler{
		StudentUsecase: su,
	}

	e.GET("/student", sc.GetStudents)
	e.GET("/student/:id", sc.GetStudent)
	e.POST("/student", sc.CreateStudent)
	// Route for updating a student
	e.PUT("/student/:id", sc.UpdateStudent)

	// Route for deleting a student
	e.DELETE("/student/:id", sc.DeleteStudent)
}
