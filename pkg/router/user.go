package router

import (
	"crud/pkg/controller"
	"crud/pkg/repository"
	"crud/pkg/usecase"
	"database/sql"
	"github.com/labstack/echo/v4"
)

func NewUserRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	ur := repository.NewUserRepository(db)
	uu := usecase.NewUserUsecase(ur)
	uc := &controller.UserControler{
		UserUsecase: uu,
	}

	e.GET("/user", uc.GetUsers)
	e.GET("/user/:id", uc.GetUser)
	e.POST("/user", uc.CreateUser)
	e.PUT("/user/:id", uc.UpdateUser)
	e.DELETE("/user/:id", uc.DeleteUser)
}
