package dto

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"regexp"
)

type UserDTO struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Address  string `json:"address"`
}

func (u UserDTO) Validation() error {
	err := validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required),
		validation.Field(&u.Email, validation.Required, validation.Match(regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`))),
		validation.Field(&u.Password, validation.Required),
		validation.Field(&u.Address, validation.Required))

	if err != nil {
		return err
	}
	return nil
}
