package controller

import (
	"crud/pkg/domain"
	"crud/pkg/dto"
	"crud/shared/response"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strconv"
)

type UserControler struct {
	UserUsecase domain.UserUsecase
}

func (uc *UserControler) GetUsers(c echo.Context) error {
	resp, err := uc.UserUsecase.GetUsers()
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserControler) GetUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := uc.UserUsecase.GetUser(id)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, "id user not found", nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserControler) CreateUser(c echo.Context) error {
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	//// Periksa apakah email sudah ada dalam database
	if _, err := uc.UserUsecase.GetUserByEmail(userdto.Email); err == nil {
		return response.SetResponse(c, http.StatusBadRequest, "user already exists", nil)
	}

	// Menghash password sebelum disimpan
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(userdto.Password), bcrypt.DefaultCost)
	userdto.Password = string(hashedPassword)

	if err := uc.UserUsecase.CreateUser(userdto); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserControler) UpdateUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if _, err := uc.UserUsecase.GetUser(id); err != nil {
		return response.SetResponse(c, http.StatusNotFound, "user not found", nil)
	}
	if err := uc.UserUsecase.UpdateUser(id, userdto); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserControler) DeleteUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	if _, err := uc.UserUsecase.GetUser(id); err != nil {
		return response.SetResponse(c, http.StatusNotFound, "user not found", nil)
	}
	if err := uc.UserUsecase.DeleteUser(id); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}
