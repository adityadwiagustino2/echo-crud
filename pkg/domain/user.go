package domain

import "crud/pkg/dto"

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Address  string `json:"address"`
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req User) error
	GetUserByEmail(email string) (User, error)
	UpdateUser(id int, req User) error
	DeleteUser(id int) error
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req dto.UserDTO) error
	GetUserByEmail(email string) (User, error)
	UpdateUser(id int, req dto.UserDTO) error
	DeleteUser(id int) error
}
