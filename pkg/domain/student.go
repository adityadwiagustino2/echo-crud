package domain

import "crud/pkg/dto"

type Student struct {
	Id         int    `json:"id"`
	Fullname   string `json:"fullname"`
	Address    string `json:"address"`
	Birthdate  string `json:"birthdate"`
	Class      string `json:"class"`
	Batch      int    `json:"batch"`
	SchoolName string `json:"school_name"`
}

type StudentRepository interface {
	GetStudents() ([]Student, error)
	GetStudent(id int) (Student, error)
	CreateStudent(req Student) error
	UpdateStudent(id int, req Student) error
	DeleteStudent(id int) error
}

type StudentUsecase interface {
	GetStudents() ([]Student, error)
	GetStudent(id int) (Student, error)
	CreateStudent(req dto.StudentDTO) error
	UpdateStudent(id int, req dto.StudentDTO) error
	DeleteStudent(id int) error
}
