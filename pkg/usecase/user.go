package usecase

import (
	"crud/pkg/domain"
	"crud/pkg/dto"
	"github.com/mitchellh/mapstructure"
)

type UserUsecase struct {
	UserRepository domain.UserRepository
}

func NewUserUsecase(userRepository domain.UserRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository: userRepository,
	}
}

func (uu UserUsecase) GetUsers() ([]domain.User, error) {
	return uu.UserRepository.GetUsers()
}

func (uu UserUsecase) GetUser(id int) (domain.User, error) {
	return uu.UserRepository.GetUser(id)
}

func (uu UserUsecase) CreateUser(req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	return uu.UserRepository.CreateUser(user)
}

func (uu UserUsecase) GetUserByEmail(email string) (domain.User, error) {
	return uu.UserRepository.GetUserByEmail(email)
}

func (uu UserUsecase) UpdateUser(id int, req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	return uu.UserRepository.UpdateUser(id, user)
}

func (uu UserUsecase) DeleteUser(id int) error {
	return uu.UserRepository.DeleteUser(id)
}
   