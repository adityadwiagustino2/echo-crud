package cmd

import (
	"crud/pkg/router"
	"crud/shared/db"
	"github.com/labstack/echo/v4"
)

// func RunServer()  {
// 	e := echo.New()
// 	db.NewInstanceDb()

// 	e.Logger.Error(e.Start( ":8080"))
// }

func RunServer() {
	e := echo.New()
	g := e.Group("")

	Apply(e, g)
	e.Logger.Error(e.Start(":8080"))
}

func Apply(e *echo.Echo, g *echo.Group) {
	db := db.NewInstanceDb()
	router.NewStudentRouter(e, g, db)
	router.NewUserRouter(e, g, db)
}
